const functions = require("firebase-functions");
const admin = require("firebase-admin");
admin.initializeApp();

//
// Create and Deploy Your First Cloud Functions
//
// https://firebase.google.com/docs/functions/write-firebase-functions
exports.androidPushNotification = functions.firestore.document("Notifications/{docId}").onCreate(
    (snapshot, context) => {
      admin.messaging().sendToTopic(
          "new_user_forums",
          {
            notification: {
              title: "Codeible.com",
              body: "Notification Tutorial",
            },
          }
      );
    }
);
